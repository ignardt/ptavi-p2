import sys
import computecount

C = computecount.Compute()


def process_csv(fichero): # Función que lee el fichero
    with open(fichero, 'r') as file:
        for line in file:
            try:
                read_line(line)
            except (ValueError, KeyError):
                print("Bad format")

def read_line(line):  # Función que muestra los resultados de las operaciones de línea en línea
    obj = line.rstrip().split(',')
    if len(obj) == 1:
        C.default = int(obj[0])
        print("Default: " + (obj[0]))
    elif len(obj) == 2:
        if obj[0] == "power":
            result = C.power(float(obj[1]), C.default)
            print(result)
        elif obj[0] == "log":
            result = C.log(float(obj[1]), C.default)
            print(result)
        else:
            print("Bad format")

    elif len(obj) == 3:
        if obj[0] == "power":
            result = C.power(float(obj[1]), float(obj[2]))
            print(result)
        elif obj[0] == "log":
            result = C.log(float(obj[1]), float(obj[2]))
            print(result)
        else:
            print("Bad format")
    else:
        print("Bad format")


if __name__ == "__main__":
    try:
        fichero = sys.argv[1]
    except IndexError:
        print("Error: Execute with the name of file to process as argument")
        sys.exit(-1)
    try:
        process_csv(fichero)
    except FileNotFoundError:
        print(f"Error: File {fichero} does not exist")
        sys.exit(-1)
